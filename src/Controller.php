<?php
namespace Gelderblominternet\Blueprintphp;

use Symfony\Component\HttpFoundation\Response;
use League\Plates\Engine;

/**
 * Class Controller
 * @package Gelderblominternet\Blueprintphp
 */
class Controller {

    /**
     * @var Engine $templates Template engine
     */
    public $templates;

    /**
     * @var Response $response HttpFoundation response
     */
    public $response;

    /**
     * Controller constructor
     */
    public function __construct()
    {
        $this->templates = new Engine(getenv('BASE_DIR').'resources/templates');
        $this->response = new Response(
            '',
            Response::HTTP_NOT_FOUND,
            array('content-type' => 'text/html')
        );
    }

}