<?php
namespace Gelderblominternet\Blueprintphp;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class Blueprint
 * @package Gelderblominternet\Blueprintphp
 */
class Blueprint {

    /**
     * @var array $bundles Code bundles
     */
    private $bundles = [];

    /**
     * @var array $services Singleton services
     */
    private $services = [];

    /**
     * @var Symfony\Component\HttpFoundation\Request $request
     */
    public $request;

    /**
     * Call this method to get singleton
     *
     * @return Gelderblominternet\Blueprintphp\Blueprint
     */
    public static function instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new self();
        }
        return $inst;
    }

    /**
     * Blueprint constructor
     */
    public function __construct()
    {
        $this
            ->buildRequest();
    }

    /**
     * Build request from globals
     * @return $this
     */
    private function buildRequest()
    {
        $this->request = Request::createFromGlobals();
        return $this;
    }

    /**
     * Set singleton service
     * @param $key
     * @param $object
     * @return $this
     */
    public function setService($key, $object)
    {
        $this->services[$key] = $object;
        return $this;
    }

    /**
     * Get singleton service
     * @param $key
     * @return mixed
     */
    public function getService($key)
    {
        return $this->services[$key];
    }

    /**
     * Get singleton service
     * @param $key
     * @return mixed
     */
    public function service($key)
    {
        return $this->getService($key);
    }

    /**
     * @param $bundle
     * @return $this
     */
    public function addBundle($bundle)
    {
        $this->bundles[] = $bundle;
        return $this;
    }

    /**
     * @param $bundles
     * @return $this
     */
    public function addBundles($bundles)
    {
        $this->bundles = array_merge($this->bundles, $bundles);
        return $this;
    }

    /**
     * Add source bundle
     * @return array
     */
    public function getBundles()
    {
        return $this->bundles;
    }

    /**
     * Load source bundle route configs
     * @return $this
     */
    public function loadBundleRoutes()
    {
        foreach ($this->bundles AS $bundle) {
            if (file_exists(getenv('BASE_DIR').'src/'.$bundle.'/config/routes.php')) {
                require_once getenv('BASE_DIR').'src/'.$bundle.'/config/routes.php';
            }
        }
        return $this;
    }

    /**
     * Prepare request for response
     * @param $response Symfony\Component\HttpFoundation\Response
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function prepare($response)
    {
        $response->prepare($this->request);
        return $response;
    }

}
