<?php
namespace Gelderblominternet\Blueprintphp;

use \Iterator;
use \PDO;
use \PDOException;
use \Countable;

/**
 * Class Orm
 * @package Gelderblominternet\Blueprintphp
 */
class Orm implements Iterator, Countable {

    /**
     * @var array Single row data
     */
    private $data = [];

    /**
     * @var array Multi row
     */
    private $list = [];

    /**
     * @var null|string Database name
     */
    public $db = null;

    /**
     * @var string Table name
     */
    public $table;

    /**
     * Factory
     * @param $class Class reference \Class\Name::class
     * @return object
     */
    public static function factory($class)
    {
        $object = new $class;
        return $object;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * @param $name
     * @return null|string|object
     */
    public function __get($name)
    {
        // Simple fields
        if (isset($this->data[$name])) {
            return $this->data[$name];
            // Relation ships
        } elseif (isset($this->relations[$name])) {
            if ($this->relations[$name]['relationship'] == 'hasOne') {
                $object = Orm::factory($this->relations[$name]['foreign']['model'])->find([ $this->relations[$name]['foreign']['column'] => $this->data[$this->relations[$name]['column']] ]);
                $this->__set($name, $object);
                return $this->__get($name);
            } elseif ($this->relations[$name]['relationship'] == 'hasMany') {
                $where = [ $this->relations[$name]['foreign']['column'] => $this->data[$this->relations[$name]['column']] ];
                if (isset($this->relations[$name]['foreign']['where']) && count($this->relations[$name]['foreign']['where']) > 0) {
                    $where = array_merge($where, $this->relations[$name]['foreign']['where']);
                }
                $objects = Orm::factory($this->relations[$name]['foreign']['model'])->findAll($where);
                $this->__set($name, $objects);
                return $this->__get($name);
            }
            // Unknown
        } else {
            // Exception unloaded model?
        }
        return null;
    }

    /**
     * Orm constructor
     * @param int $id
     */
    public function __construct($id = 0)
    {
        if ($id > 0) {
            $this->findId($id);
        }
    }

    /**
     * Countable
     * @return int
     */
    public function count() {
        return count($this->list);
    }

    /**
     * Use DB
     * @param $db
     */
    public function useDb()
    {
        if ($this->db !== null) {
            Blueprint::instance()->getService('database')->pdo->query('USE `'.$this->db.'`');
        }
    }

    /**
     * Find single row based on primary key
     * @param int $id
     * @return $this
     */
    public function findId($id)
    {
        return $this->find([ $this->primaryKeyColumn() => $id ]);
    }

    /**
     * Update object data in database
     * @param array $columns Limit updating to these columns | empty for all visible columns
     * @return $this
     */
    public function save($columns = [])
    {
        // Set DB use
        $this->useDb();
        // Get primary key
        $primaryKey = $this->primaryKeyColumn();
        // Update
        if (isset($this->data[$primaryKey]) && $this->data[$primaryKey] > 0) {
            $update = [];
            if (count($columns) == 0) {
                $columns = $this->visibleColumns();
            }
            foreach ($columns AS $column) {
                $update[$column] = $this->$column;
                if (isset($this->columns[$column]['type']) && $this->columns[$column]['type'] == 'json') {
                    $update[$column] = json_encode($update[$column]);
                }
            }
            Blueprint::instance()->getService('database')->update($this->table, $update, [$primaryKey => $this->$primaryKey]);
            // Insert
        } else {
            $insert = [];
            foreach ($this->data AS $column => $value) {
                if (!is_object($value)) {
                    $insert[$column] = $value;
                    if (isset($this->columns[$column]['type']) && $this->columns[$column]['type'] == 'json') {
                        $insert[$column] = json_encode($insert[$column]);
                    }
                }
            }
            Blueprint::instance()->getService('database')->insert($this->table, $insert);
            $this->$primaryKey = Blueprint::instance()->getService('database')->id();
        }
        return $this;
    }

    /**
     * Delete row from database
     * @return $this
     */
    public function delete()
    {
        // Set DB use
        $this->useDb();
        // Get primary key
        $primaryKey = $this->primaryKeyColumn();
        // Delete
        Blueprint::instance()->getService('database')->delete($this->table, [$primaryKey => $this->$primaryKey]);
    }

    /**
     * Find single row
     * @param array $where
     * @param null|array $columns
     * @param array $join
     * @return $this
     */
    public function find($where = [], $columns = null, $join = [])
    {
        // Set DB use
        $this->useDb();
        // Columns default fallback
        if ($columns == null && count($join) == 0) {
            $columns = $this->visibleColumns();
        } elseif ($columns == null && count($join) > 0) {
            $columns = $this->visibleColumns(true);
        }
        if (count($join) == 0) {
            $data = Blueprint::instance()->getService('database')->select($this->table, $columns, $where);
        } else {
            $data = Blueprint::instance()->getService('database')->select($this->table, $join, $columns, $where);
        }
        // Data found
        if (isset($data[0])) {
            $this->setData($data[0]);
        }
        return $this;
    }

    /**
     * Medoo raw implementation
     * @param $query
     * @return $this
     */
    public function raw($query)
    {
        $this->useDb();
        try {
            $results = Blueprint::instance()->getService('database')->pdo->query($query);
            $data = $results->fetch(PDO::FETCH_ASSOC);
            $this->setData($data);
            return $this;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Medoo raw implementation
     * @param $query
     * @return $this
     */
    public function rawList($query, $params = [])
    {
        $this->useDb();
        try {
            #Blueprint::instance()->getService('database')->debug();
            $data = Blueprint::instance()->getService('database')->query($query, $params);
            // Reset list
            $this->list = [];
            // Each result row
            foreach ($data AS $self) {
                // Create object
                $obj = Orm::factory(get_called_class());
                $obj->setData($self);
                $this->list[] = $obj;
            }
            return $this;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Count database results
     * @param array $where
     * @return int
     */
    public function countAll($where = [])
    {
        return Blueprint::instance()->getService('database')->count($this->table, $where);
    }

    /**
     * Set object data
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        if (is_array($data)) {
            foreach ($data AS $column => $value) {
                if (isset($this->columns[$column]['type']) && $this->columns[$column]['type'] == 'json') {
                    $data[$column] = json_decode(utf8_encode(trim($value, '"')), true);
                }
            }
            $this->data = $data;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Find multiple rows
     * @param array $where
     * @param array $additionalColumns
     * @param array $join
     * @return $this
     */
    public function findAll($where = [], $additionalColumns = [], $join = [])
    {
        // Set DB use
        $this->useDb();
        // Retrieve data
        if (count($join) == 0) {
            $columns = array_merge($this->visibleColumns(), $additionalColumns);
            $data = Blueprint::instance()->getService('database')->select($this->table, $columns, $where);
        } else {
            $columns = array_merge($this->visibleColumns(true), $additionalColumns);
            $data = Blueprint::instance()->getService('database')->select($this->table, $join, $columns, $where);
        }
        // Reset list
        $this->list = [];
        // Each result row
        foreach ($data AS $self) {
            // Create object
            $obj = Orm::factory(get_called_class());
            $obj->setData($self);
            $this->list[] = $obj;
        }
        return $this;
    }

    /**
     * Get visible columns
     * @return array
     */
    public function visibleColumns($addTable = false)
    {
        $visible = [];
        foreach ($this->columns AS $column => $conf) {
            if (!isset($conf['visible']) || $conf['visible'] == true) {
                if ($addTable == true) {
                    $visible[] = $this->table.'.'.$column;
                } else {
                    $visible[] = $column;
                }
            }
        }
        return $visible;
    }

    /**
     * Get primary key
     * @return string
     */
    private function primaryKeyColumn()
    {
        $primaryKey = null;
        foreach ($this->columns AS $column => $conf) {
            if (isset($conf['primary']) && $conf['primary'] == true) {
                $primaryKey = $column;
            }
        }
        return (($primaryKey == null)?'id':$primaryKey);
    }

    /**
     * Iterator rewind
     */
    public function rewind()
    {
        reset($this->list);
    }

    /**
     * Iterator current obj
     * @return Orm
     */
    public function current()
    {
        return current($this->list);
    }

    /**
     * Iterator current key
     * @return int
     */
    public function key()
    {
        return key($this->list);
    }

    /**
     * Iterator next
     * @return Orm
     */
    public function next()
    {
        return next($this->list);
    }

    /**
     * Iterator valid key
     * @return bool
     */
    public function valid()
    {
        $key = key($this->list);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
    }

}